/**
* Filename: PlayerCharacter.h
* Purpose:	PlayerCharacter class declaration
* NOTE:		Acting as a concrete class for the character class.
* Group E Productions
*
* Contributors:
*		Matt Koenig
*		Claire Fritzler
*		Tylor Faoro
*/
#pragma once
#ifndef PLAYER_CHARACTER_H
#define PLAYER_CHARACTER_H

#include "Character.h"
#include "CharacterDecorator.h"
#include <string>
#include <vector>

/*
struct Item_Types{
    StaffDecorator staff;
    SwordDecorator sword;
};
*/

class PlayerCharacter :	public Character{
public:
   /**
   * Constructor for player character
   * @param name sets name of character 
   */
    PlayerCharacter(std::string name);
   /**
   * Destructor of PlayerCharacter
   */
    ~PlayerCharacter();
    /**
    * Method to @return health of the character
    */
    int getHealth();
    /**
    * Method to set health
    * @param health_value is the value the players health is set to
    */
    void setHealth(int health_value);
    /**
    * 
    */
	void setHealthBuffs(int healthbuff_value);
	/**
    *
    */
    int getHealthBuffs();
    /**
    *
    */
	void useCurrentHealthBuffs();
    /**
    * Method for a player to use an item
    */
    void useItem();
    /**
    * Method to @return the type of the character (wizard or warrior)
    */
    std::string getCharacterType();
    /**
    *  Method to set the type of the player (wizard or warrior)
    * @param player_type is the type of the player
    */
    void setPlayerType(std::string player_type);
	/**
    * Virtual function to set name of the player
    * @param n is name of the player
    */
    virtual void setName(std::string n);
    /**
    * Virtual function to @return name of the player
    */
	virtual std::string getName();
    /**
    * @return the description of the character
    */
    std::string getDescription();
    /**
    * @return the action taken by the player
    */
    std::string getAction();
    /**
    * virtual method to unlock door
    * @return true if player has key to unlock door
    * @return false if player does not have key in inventory
    */
	virtual bool getUnlock(); // Bool for key use.
    /**
    * @return attack value
    */
    int getAttack();
    /**
    * @return defense value
    */
    int getDefense();
	//Actions* action;
private:
    int health; /*< health of the player */
	bool crit; /*< whether the player can preform crit attack */
	std::string name; /*< name of the player */
    std::string PlayerType; /*< type of the player */
	// Item Description
	std::string Description; /*< description of the player */
	CharacterDecorator *CharDecorator; /*< item decorations that can be added to a player */
	
};

#endif

