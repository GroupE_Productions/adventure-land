/**
* Filename: FantasyGame.cpp
* Purpose:	Provides implementation methods for the FantasyGame Class. This class
*			acts as the main "Machine" for the game. This is where all the magic
*			happens. All steps of the story are iterated through here.
*
* NOTE:		To save lines we are using an external asset design method. Such that,
*			any dialogue for the game that may be edited at a later date are being
*			stored inside external *.dat files. Inside of the Assets folder.
*
* Group E Productions
*
* Contributors:
*		Matt Koenig
*		Claire Fritzler
*		Tylor Faoro
*/

#include "FantasyGame.h"
#include "Environments.h"
#include "Actions.h"
#include <conio.h>

/**
* Purpose:	Constructor. This is where the game is activated. Boolean value of
*			alive is set to true and on we go with the game.
*
* Author:	Tylor Faoro
*/
FantasyGame::FantasyGame() {
	alive = true;
}

/**
* Purpose:	Increments the private variable "moveCounter" which tracks the hero's
*			current stage of the game. This method will iterate the hero onto the
*			next stage of his/her journey.
*
* Author:	Tylor Faoro
*/
void FantasyGame::NextMove() {
	this->moveCounter++;
}

/**
* Purpose:	Used as the main game loop stop condition. This ensures as long as the hero
*			is alive, then the game will go on.
*
* Author:	Tylor Faoro
*/
bool FantasyGame::isOver() {
	alive = false;
	return alive;
}

/**
* Purpose:	Is activated if the hero dies in the game. This method will end the game
*			immediately as it triggers the main game loops stop condition.
*
* Author:	Tylor Faoro
*/
void FantasyGame::kill() {
	alive = false;
}

/**
* Purpose:	Utility Method. This method will take in a *.dat file (or any file for that matter) and pass it to
*			a method calld "PrintFileContents() inside of the Game class. Used for inputing dialogue and ASCII art.
*
* Author:	Tylor Faoro
*/
void FantasyGame::OutputFile(std::string fileName) {
	std::string DirectoryPath = "../adventure-land/Assets/";	//:: Directory location of Assets Folder. **REQUIRED**

	std::ifstream Reader(DirectoryPath + fileName);
	std::string Title = g->PrintFileContents(Reader);
	std::cout << Title;
}

/**
* Purpose:	To take in an Question to ask and an input variable for CIN to work with.
* NOTE:		This method is only used once. It was originally intended to act as a utility function
*			to help with user input, but was never implemented.
*
* Author:	Tylor Faoro
*/
void FantasyGame::EnterInput(std::string question, int inputVar) {
	int error = 0;
	do {
		std::cout << question << ": ";
		if (std::cin.fail()) {
			std::cout << "Please enter a valid input.";
			error = 1;
			std::cin.clear();
			std::cin.ignore(80, '\n');

		}
		std::cin >> inputVar;
	} while (error == 1);

}

/**
* Purpose: Generates the game, and establishes all of the trips for our journey.
*
* Author: Tylor Faoro
*/
void FantasyGame::play() {
	alive = true;
	InitGame();
	while (alive) 
	{
		switch (moveCounter) {
		default:
			ClearScreen();
			InitGame();
			std::cin >> moveCounter;
			break;
		case 0:
			ClearScreen();
			MoveOne();
			break;
		case 1:
			ClearScreen();
			MoveTwo();
			break;
		case 2:
			ClearScreen();
			MoveThree();
			break;
		case 3:
			ClearScreen();
			MoveFour();
			break;
		case 4:
			ClearScreen();
			MoveFive();
			break;
		case 5:
			ClearScreen();
			MoveSix();
			break;
		case 6:
			ClearScreen();
			MoveSeven();
			break;
		case 7:
			ClearScreen();
			MoveEight();
			break;
		case 8:
			ClearScreen();
			MoveNine();
			break;
		case 9:
			ClearScreen();
			MoveTen();
			break;

		case 10:
			ClearScreen();
			FinishGame();
			break;
		}

	}
}

void FantasyGame::InitGame() {
	OutputFile("Title.dat");
	OutputFile("Menu.dat");
	EnterInput("Enter Option", moveOption);
	std::cout << " " << std::endl;
	switch (moveOption) {
	case Play:
		ClearScreen();
		MoveOne();
		//MoveFive();	//:: Forcing my way to the end
		break;
	case Exit:
		alive = false;
		exit(1);
		ClearScreen();
		OutputFile("Title.dat");
		std::cout << "Thank you for playing!";
		break;
	}
}

void FantasyGame::MoveOne() {

	ClearScreen();
	OutputFile("Title.dat");
	std::cout << "Chapter One" << std::endl;
	OutputFile("MoveOne.dat");

	//:: Randomize the Environment and Instantiate Object
	int randVar = (rand() % 4);
	std::string strName;
	Environments *env = new Environments(randVar);


	std::cout << "What is your name? ";
	std::cin >> strName;
	p->setName(strName);
	std::cout << std::endl << "Well " << p->getName() << " You are in for quite an adventure..." << std::endl;
	PressEnter();

	// Castles
	ClearScreen();
	OutputFile("MoveOne1.dat");
	PressEnter();

	//Mountains
	ClearScreen();
	std::cout << std::endl << std::endl;
	OutputFile("MoveOne2.dat");
	PressEnter();

	//Forests
	ClearScreen();
	std::cout << std::endl << std::endl;
	OutputFile("MoveOne3.dat");
	PressEnter();

	//Lakes
	ClearScreen();
	std::cout << std::endl << std::endl;
	OutputFile("MoveOne4.dat");
	PressEnter();

	//Dragons
	ClearScreen();
	std::cout << std::endl << std::endl;
	OutputFile("MoveOne5.dat");
	PressEnter();

	//Evil Creatures
	ClearScreen();
	std::cout << std::endl << std::endl;
	OutputFile("MoveOne6.dat");
	PressEnter();

	//The voice in the darkness
	ClearScreen();
	std::cout << std::endl << std::endl;
	OutputFile("MoveOne7.dat");
	PressEnter();

	// The hero awakens
	ClearScreen();
	std::string PlayerType;
	OutputFile("EnvDarkForest.dat");
	std::cout << p->getName() << " you awaken in a " << env->getEnvName() << " you can here the crunching of leaves and branches";
	std::cout << "in the distance. The sound is getting closer and you can hear a wet heavy breathing coming from whatever";
	std::cout << "creature is making the noise. It is a good thing you are a....  (Warrior?, Wizard?) " << std::endl;
	std::cin >> PlayerType;
	PlayerType[0] = toupper(PlayerType[0]);

	while (PlayerType != "Warrior" && PlayerType != "Wizard" && PlayerType != "Help")
	{
		std::cout << p->getName() << " dont be silly nobody is a " << PlayerType << " around here...	" << std::endl;
		std::cout << "It is a good thing you are a....  ";
		std::cin >> PlayerType;
	}
	p->setPlayerType(PlayerType);

	if (PlayerType == "Warrior")
	{
		ClearScreen();
		p = new SwordDecorator(p);
		OutputFile("Warrior.dat");
		std::cout << "You reach down between your legs and grab your sword whatever is coming will surely regret..." << std::endl;
		PressEnter();
	}
	if (PlayerType == "Wizard")
	{
		ClearScreen();
		p = new StaffDecorator(p);
		OutputFile("Wizard.dat");
		std::cout << "You  whisper quietly 'rise baculus' and with only a whisper of noise your staff rises to your hand..." << std::endl;
		PressEnter();
	}
	if (PlayerType == "Help")
	{
		std::cout << "ToDo: Impliment help functionality" << std::endl;
		std::cout << "Select either Wizard or Warrior to continue";
		PressEnter();
	}
	ClearScreen();
	OutputFile("EnvStars.dat");
	PressEnter();

	//:: Proceed to Chapter 2.
	NextMove();
}

void FantasyGame::MoveTwo() {

	NPCCharacter->setPlayerType("Ogre");
	NPCCharacter->setName("Jim the Ogre");
	NPCCharacter = new ClubDecorator(NPCCharacter);

	// The Ogre encounter
	ClearScreen();
	if (p->getCharacterType() == "Warrior") {
		OutputFile("MoveTwoWarriorOne.dat");
		PressEnter();
	}
	else {
		OutputFile("MoveTwoWizardOne.dat");
		PressEnter();
	}
	//:: A Fight!
	ClearScreen();
	std::string moveOptStr;
	OutputFile("MoveTwo2.dat");
	std::cout << std::endl;
	std::cout << p->getName() << "! you must act quickly, do you fight or flee?" << std::endl << "Choose: ";
	std::cin >> moveOptStr;
	moveOptStr[0] = toupper(moveOptStr[0]);

	while (moveOptStr != "Fight" && moveOptStr != "Flee" && moveOptStr != "Help")
	{
		std::cout << p->getName() << " dont be silly you couldnt possibly " << moveOptStr << std::endl;
		std::cout << p->getName() << "! you must act quickly, do you fight or flee?" << std::endl << "Choose: ";
		std::cin >> moveOptStr;
		moveOptStr[0] = toupper(moveOptStr[0]);
	}

	//:: Choose to Fight
	if (moveOptStr == "Fight" || moveOptStr == "Attack") {
		action->attack(p, NPCCharacter);
		ClearScreen();
		DrawStats(p);
		std::cout << std::endl << "You move over to the lifeless body of " << NPCCharacter->getName() << std::endl;
		std::cout << "He wont be needing these things..." << std::endl;
		p = action->getItem(p, "Chain Mail Shirt");
		std::cout << "Chain mail has increased your Defense by 10" << std::endl;
		p = action->getItem(p, "Health Potion");
		PressEnter();
	}
	//:: Choose to Run Away.
	if (moveOptStr == "Flee" || moveOptStr == "Run") {
		//:: Roll for a 75% chance of success.	
		bool roll = Game::DiceRoll(75);

		//:: Roll was successful
		if (roll) {
			OutputFile("Escape.dat");
			PressEnter();
		}
		//:: You be dead.
		else {
			ClearScreen();
			OutputFile("Death.dat");
			std::cout << "You're Dead!" << std::endl;
			std::cout << "Game Over!";
			PressEnter();
			kill();
		}
	}
	if (moveOptStr == "Help") {
		std::cout << "HELP: Select one of the following options: \"Attack\", \"Fight\", \"Run\", or \"Flee\" " << std::endl;
	}

	//:: Proceed to Chapter 3
	NextMove();
}


void FantasyGame::MoveThree() {
	ClearScreen();
	DrawStats(p);
	Environments *env = new Environments("dark cave");
	OutputFile("MoveThree.dat");
	PressEnter();
	//std::cout << p->getName() << std::endl;
	/*
	std::cout << p->getHealth() << std::endl;
	std::cout << "Health Buff starting: " << p->getHealthBuffs() << " Health Starting: " << p->getHealth() << std::endl;
	p = action->getItem(p, "Health Potion");
	//p = new HealthPotionDecorator(p);
	std::cout << "Health Buff ending: " << p->getHealthBuffs() << " Health ending: " << p->getHealth() << std::endl;
	p->useCurrentHealthBuffs();
	p = new UsedHealthPotionDecorator(p);
	std::cout << "Health Buff after use: " << p->getHealthBuffs() << " Health after use: " << p->getHealth() << std::endl;
	//std::cout <<
	PressEnter();
	*/

	// Fall asleep in the cave.
	ClearScreen();
	OutputFile("MoveThree2.dat");
	PressEnter();

	// Wake up in the dark.
	std::string strDirection;
	ClearScreen();
	OutputFile("MoveThree3.dat");
	std::cout << "Which direction will you go? ";
	std::cin >> strDirection;
	strDirection[0] = toupper(strDirection[0]);

	while (strDirection != "Left" && strDirection != "Right"  && strDirection != "Forward" && strDirection != "Help")
	{
		std::cout << p->getName() << " what is your problem you cant go " << strDirection << std::endl;
		std::cout << "Do you need some \"Help\"?" << std::endl;
		std::cout << "Which direction will you go? ";
		std::cin >> strDirection;
	}

	std::cout << "You move cautionsuly " << strDirection << std::endl;
	PressEnter();

	//:: 1/3 chance that that was not the direction you did not want to take
	bool roll = Game::DiceRoll(80);
	//:: If the roll was successful. Either the player moves to the next level, or they encounter a peasant
	if (roll)
	{
		ClearScreen();
		std::cout << "It turns out the sound you heard behind you was just a harmless peasant who enjoys hanging out in dark caves." << std::endl;
		OutputFile("MoveThree_Peasant.dat");
		// This is just an example question, will perfect later...
		std::string answerStr;
		std::cout << "What is your name?... ";
		std::cin >> answerStr;
		std::cout << std::endl;
		answerStr[0] = toupper(answerStr[0]);
		//:: If you answered the name question correctly
		if (answerStr == p->getName())
		{
			std::cout << "What is your favorite color?... ";
			// Accepts multi word string.
			std::string answerStr2;
			std::getline(std::cin >> answerStr2, answerStr2);
			std::cout << std::endl;

			std::cout << "What is your quest?... ";
			std::string answerStr3;
			std::cin >> answerStr3;
			std::getline(std::cin >> answerStr3, answerStr3);
			std::cout << std::endl;

			std::cout << "Are you sure?... (Yes, No)";
			std::string answerStr4;
			std::cin >> answerStr4;
			std::cout << std::endl;
			answerStr4[0] = toupper(answerStr4[0]);
			if (answerStr4 == "Yes")
			{
				OutputFile("MoveThree_Right_Answer.dat");
				p = new KeyDecorator(p);
				NextMove();
			}
			else
			{
				ClearScreen();
				OutputFile("MoveThree_Wrong_Answer.dat");
				std::cout << "Game Over!" << std::endl;
				PressEnter();
				kill();
			}
		}
		else
		{
			ClearScreen();
			OutputFile("MoveThree_Wrong_Answer.dat");
			std::cout << "Game Over!" << std::endl;
			PressEnter();
			kill();
		}
	}

	//:: That was not the right way... You be dead
	else {
		ClearScreen();
		OutputFile("Death.dat");
		OutputFile("MoveThree_Wrong_Direction.dat");
		std::cout << "Game Over" << std::endl;
		PressEnter();
		kill();
	}
}

void FantasyGame::MoveFour() {
	ClearScreen();
	/** Player is in a new random environment */

	srand(time(0));
	int randVar = (rand() % 2);

	std::string strName;
	Environments *envFour = new Environments(randVar);
	std::string envFourName = envFour->getEnvName();
	std::cout << envFourName << std::endl;
	ClearScreen();
	if (envFourName == "Dark Cave")
	{
		OutputFile("MoveFour_DarkCave.dat");
		std::cout << "The only thing to do now is explore, which direction will you go? " << std::endl << "(Left, Right or Forward) ";
		std::string strDirection;
		std::cin >> strDirection;
		strDirection[0] = toupper(strDirection[0]);

		while (strDirection != "Left" && strDirection != "Right"  && strDirection != "Forward" && strDirection != "Help")
		{
			std::cout << p->getName() << " what is your problem you cant go " << std::endl << strDirection << std::endl;
			std::cout << "Do you need some \"Help\"?" << std::endl;
			std::cout << "Which direction will you go? ";
			std::cin >> strDirection;
			strDirection[0] = toupper(strDirection[0]);
		}
		std::cout << "Moving " << strDirection << " you encounter a hole in the floor. Do you explore it? (Yes, No)";
		std::string strReturn;
		std::cin >> strReturn;
		strReturn[0] = toupper(strReturn[0]);

		while (strReturn != "Yes" && strReturn != "No"  && strReturn != "Help")
		{
			std::cout << p->getName() << " its a yes or no question " << strReturn << " is just not an answer" << std::endl;
			std::cout << "Do you need some \"Help\"?" << std::endl;
			std::cout << "Which direction will you go? ";
			std::cin >> strReturn;
			strReturn[0] = toupper(strReturn[0]);
		}

		if (strReturn == "Yes")
		{
			// The player dies
			bool roll = Game::DiceRoll(20);
			if (roll)
			{
				ClearScreen();
				std::cout << "As you wriggle your way into the hole you slip and hit your head." << std::endl;
				OutputFile("Death.dat");
				std::cout << "Game Over!" << std::endl;
				PressEnter();
			}
			else
			{
				ClearScreen();
				OutputFile("MoveFour_DarkCave2.dat");
				PressEnter();
				NextMove();
			}
		}
		if (strReturn == "No")
		{
			// The player dies
			bool roll = Game::DiceRoll(20);
			if (roll)
			{
				ClearScreen();
				std::cout << "As you walk away from the hole a rock slips from a ledge and crushes your head." << std::endl;
				OutputFile("Death.dat");
				std::cout << "Game Over!" << std::endl;
				PressEnter();
			}
			else
			{
				ClearScreen();
				OutputFile("MoveFour_DarkCave3.dat");
				PressEnter();
				ClearScreen();
				OutputFile("key.dat");
				p = new KeyDecorator(p);
				std::cout << "You use your new found key to unlock the nearby door, that was lucky... " << std::endl;
				PressEnter();
				NextMove();
			}
		}

	}

	else
	{
		OutputFile("MoveFour_Catacomb.dat");
		PressEnter();
		bool roll = Game::DiceRoll(30);
		if (roll)
		{
			ClearScreen();
			OutputFile("Key.dat");
			p = new KeyDecorator(p);
			std::cout << "You use your new found key to unlock the door, that was lucky... " << std::endl;
			PressEnter();
			NextMove();
		}
		else
		{
			bool roll = Game::DiceRoll(70);
			if (roll)
			{
				ClearScreen();
				OutputFile("MoveFour_Catacomb2.dat");
				PressEnter();
				NextMove();
			}
			else
			{
				ClearScreen();
				std::cout << "Looks like you will be spending the rest of your life in this jail cell" << std::endl;
				OutputFile("Death.dat");
				std::cout << "Game Over!" << std::endl;
				PressEnter();
			}
		}
	}
}

void FantasyGame::MoveFive() {
	/** Dragon Level */
	ClearScreen();
	std::string moveStr;
	NPCCharacter->setPlayerType("Dragon");
	NPCCharacter->setName("Davy the Dragon");
	NPCCharacter = new FireDecorator(NPCCharacter);

	OutputFile("dragon.dat");
	PressEnter();

	ClearScreen();
	std::cout << "You have little choice... You cant go back the dragon will surely catch you. " << std::endl;
	std::cout << "must fight or escape into the tower do you fight or escape? ";
	std::cin >> moveStr;
	moveStr[0] = toupper(moveStr[0]);
	while (moveStr != "Fight" && moveStr != "Escape")
	{
		std::cout << moveStr << std::endl;
		std::cout << p->getName() << " dont be silly  " << moveStr << " isn't a valid move " << std::endl;
		std::cout << "Do you need some \"Help\"?" << std::endl;
		std::cout << "Would you like to Run or Fight ?";
		std::cin >> moveStr;
	}

	if (moveStr == "Fight")
	{
		/** Fight the dragon */
		Actions* fight = new Actions();
		fight->attack(p, NPCCharacter);
	}

	if (moveStr == "Escape")
	{
		ClearScreen();
		std::cout << "You run to the tower gate and realize it is locked..." << std::endl;
		if (p->getUnlock() == true)
		{
			OutputFile("MoveFive_RunInside.dat");
			p = new HelmetDecorator(p);
			p = new DaggerDecorator(p);
			p = new ShieldDecorator(p);

			std::cout << "Feeling well prepared you charge back outside to fight the dragon" << std::endl;
			PressEnter();
			Actions* fight = new Actions();
			fight->attack(p, NPCCharacter);

			moveCounter++;
		}
		else
		{
			/* Fight the dragon player is stuck outside */
			std::cout << "You are trapped outside you must prepare for battle " << std::endl;
			PressEnter();
			Actions* fight = new Actions();
			fight->attack(p, NPCCharacter);
		}

	}
}


void FantasyGame::MoveSix() {
	NPCCharacter->setPlayerType("Peasant");
	NPCCharacter->setName("Garret the Blacksmith");
	NPCCharacter = new HammerDecorator(NPCCharacter);

	//:: Peasant Encounter
	ClearScreen();
	OutputFile("MoveSix.dat");
	std::string inputString;
	std::cout << "You approach the peasant cuatiously should you try and befrient him or slay him?: ";
	std::cin >> inputString;
	inputString[0] = toupper(inputString[0]);
	//:: The peasant takes you to his village and gives you a health potion
	while (inputString != "Befriend" && inputString != "Slay")
	{
		std::cout << p->getName() << " dont be silly you cant do that to a stranger... " << std::endl;
		std::cout << "Do you need some \"Help\"?" << std::endl;
		std::cout << "Would you like to Befriend or Slay him?";
		std::cin >> inputString;
	}
	if (inputString == "Befriend")
	{
		ClearScreen();
		std::cout << "You yell to the stranger.... Hello!" << std::endl << std::endl;
		std::cout << "He replies back " << p->getName() << " is that you?" << std::endl;
		OutputFile("MoveSix_Befriend.dat");
		PressEnter();
		MoveEight();
	}
	//:: You killed the peasant and must flee the scene... But you still get caught and thrown in jail
	else if (inputString == "Slay")
	{
		ClearScreen();
		OutputFile("MoveSix_Slay.dat");
		PressEnter();
		Actions* fight = new Actions();
		if(fight->attack(p, NPCCharacter) == 0)
		{
			isOver();
			std::cout << "You really should be dead here" << std::endl;
		}
		else
		{
			ClearScreen();
			OutputFile("MoveSix_Caught.dat");
			std::cout << p->getHealth() << std::endl;
			NextMove();
		}
	}
}

void FantasyGame::MoveSeven() {
	NPCCharacter->setPlayerType("Warrior");
	NPCCharacter->setName("Andrea the Almost Giant");
	NPCCharacter = new BastardSwordDecorator(NPCCharacter);

	ClearScreen();
	OutputFile("MoveSeven.dat");
	PressEnter();

	Actions* fight = new Actions();

	if (fight->attack(p, NPCCharacter) == 1)
	{
		ClearScreen();
		OutputFile("MoveSeven2.dat");
		PressEnter();
		MoveNine();
	}
	else
	{
		isOver();
	}
}

void FantasyGame::MoveEight() {
	ClearScreen();

	OutputFile("MoveEight.dat");
	PressEnter();
	ClearScreen();
	OutputFile("MoveEight2.dat");
	PressEnter();
	std::string moveOp;
	std::cout << "Will you fight the evil king (Yes/No)";
	std::cin >> moveOp;
	moveOp[0] = toupper(moveOp[0]);
	//:: The peasant takes you to his village and gives you a health potion
	while (moveOp != "Yes" && moveOp != "No")
	{
		std::cout << p->getName() << " that is not a yes no answer what do you mean? " << std::endl;
		std::cout << "Will you fight the evil king (Yes/No)";
		std::cin >> moveOp;
		moveOp[0] = toupper(moveOp[0]);
	}

	if (moveOp == "Yes")
	{
		ClearScreen();
		OutputFile("MoveEight_Accept.dat");
		p = new AmuletDecorator(p);
		PressEnter();
		MoveTen();
	}

	if (moveOp == "No")
	{
		ClearScreen();
		OutputFile("MoveEight_GetDrunk.dat");
		PressEnter();
		ClearScreen();
		OutputFile("MoveEight_Jail.dat");
		PressEnter();
		ClearScreen();
		MoveNine();
	}

}

void FantasyGame::MoveNine() {
	// Debug code;
	DebugInitWarrior();
	p = new ChainMailDecorator(p);
	p = new HealthPotionDecorator(p);
	p = new KeyDecorator(p);
	p = new HelmetDecorator(p);
	p = new DaggerDecorator(p);
	p = new ShieldDecorator(p);
	ClearScreen();
	Environments envNine = *new Environments("Jail");

	std::cout << "Well " << p->getName() << " you have gotten yourself into quite a bit of trouble" << std::endl;
	OutputFile("MoveNine.dat");
	std::string moveOp;
	std::cout << "Will you fight the evil king (Yes/No)";
	std::cin >> moveOp;
	moveOp[0] = toupper(moveOp[0]);

	while (moveOp != "Yes" && moveOp != "No")
	{
		std::cout << p->getName() << " that is not a yes no answer what do you mean? " << std::endl;
		std::cout << "Will you fight the evil king (Yes/No)";
		std::cin >> moveOp;
		moveOp[0] = toupper(moveOp[0]);
	}
	if (moveOp == "Yes")
	{
		ClearScreen();
		OutputFile("MoveNine_Release.dat");
		PressEnter();
		MoveTen();
	}

	if (moveOp == "No")
	{
		ClearScreen();
		std::cout << "Looks like you will be spending the rest of your life in this jail cell" << std::endl;
		OutputFile("Death.dat");
		std::cout << "Game Over!" << std::endl;
		alive = false;
		PressEnter();
	}
}

void FantasyGame::MoveTen() {
	//:: Start Final Stage of the journey.	
	ClearScreen();
	OutputFile("MoveTen-One.dat");
	PressEnter();
	ClearScreen();
	OutputFile("MoveTen-two.dat");
	PressEnter();
	ClearScreen();
	OutputFile("MoveTen-Three.dat");
	PressEnter();
	ClearScreen();
	OutputFile("MoveTen-Four.dat");
	//:: We Enter the Tower
	Environments *env = new Environments("Tower");

	//:: Process Ladder Climbing Action
	std::string answer = "";
	std::cout << "Would you like to \"climb\" the ladder to enter the " << env->getEnvName() << "? " << std::endl;
	std::cout << "Your Choice: ";
	std::cin >> answer;
	answer[0] = toupper(answer[0]);
	while (answer != "Climb") {
		std::cout << "Unfortunately, that isn't a valid option.\nDo you want to \"Climb\" the ladder?:";
		std::cin >> answer;
		answer[0] = toupper(answer[0]);
	}
	//:: Look up. Look Waaaay up, and I'll call Rusty.
	if (answer == "Climb") {
		Actions *climbLadder = new Actions();
		bool climbOutcome = climbLadder->climb();
		//:: You are inside the tower
		if (climbOutcome) {
			//:: Check to see if there is a monster awaiting you at the top of the ladder.
			bool fightVarOne;
			fightVarOne = g->DiceRoll(75);
			if (fightVarOne) {
				ClearScreen();
				std::cout << "FIGHT" << std::endl;
				PressEnter();
				NPCCharacter->setPlayerType("Warrior");
				NPCCharacter->setName("Two Finger Jack");
				NPCCharacter = new ClubDecorator(NPCCharacter);
				NPCCharacter = new ChainMailDecorator(NPCCharacter);
				Actions* fight = new Actions();
				if (fight->attack(p, NPCCharacter) == 1) {
					ClearScreen();
					OutputFile("Death.dat");
					std::cout << "You have won!\nYou find nothing on the corpse." << std::endl;
					PressEnter();
				}
				else {
					isOver();
				}
			}

			//:: Check if you wish to advance to retreat
			std::string answerTwo = "";
			ClearScreen();
			OutputFile("MoveTen-InsideTower.dat");
			std::cout << "Do you Advance or Retreat?" << std::endl;
			std::cout << "You Choice: ";
			std::cin >> answerTwo;
			answerTwo[0] = toupper(answerTwo[0]);
			while (answerTwo != "Advance" && answerTwo != "Retreat") {
				std::cout << "Unfortunately, \"" << answerTwo << "\" isn't a valid option.\nWill you \"Advance\" or \"Retreat\"?" << std::endl;
				std::cout << "You Choice: ";
				std::cin >> answerTwo;
				answerTwo[0] = toupper(answerTwo[0]);
			}
			if (answerTwo == "Retreat") {
				std::cout << "As you turn to retreat, you look and realize the precarious ladder you used to\n climb into this " << env->getEnvName() << " has fallen apart.\nNo Escape Is Possible." << std::endl;
				answerTwo = "Advance";
				PressEnter();
			}
			if (answerTwo == "Advance") {
				//:: Check if there is a Mage waiting for you
				bool fightVarTwo;
				fightVarTwo = g->DiceRoll(50);
				if (fightVarTwo) {
					ClearScreen();
					std::cout << "FIGHT" << std::endl;
					PressEnter();
					NPCCharacter->setPlayerType("Mage");
					NPCCharacter->setName("Archaveous Maximus");
					NPCCharacter = new ClubDecorator(NPCCharacter);
					NPCCharacter = new ChainMailDecorator(NPCCharacter);
					NPCCharacter = new StaffDecorator(NPCCharacter);
					Actions* fight = new Actions();

					if (fight->attack(p, NPCCharacter) == 1) {
						ClearScreen();
						OutputFile("Death.dat");
						std::cout << "You have won!\nYou find nothing on the corpse." << std::endl;
						PressEnter();
					}
					else {
						isOver();
					}
				}

				ClearScreen();
				OutputFile("MoveTen-Hallway.dat");
				PressEnter();

				//:: Check to see if there is a monster awaiting you down the hallway.
				bool fightVarThree;
				fightVarThree = g->DiceRoll(50);
				if (fightVarThree) {
					ClearScreen();
					std::cout << "FIGHT" << std::endl;
					PressEnter();
					NPCCharacter->setPlayerType("Rogue");
					NPCCharacter->setName("Rotanis Hairypalms");
					NPCCharacter = new DaggerDecorator(NPCCharacter);
					NPCCharacter = new ChainMailDecorator(NPCCharacter);
					Actions* fight = new Actions();
					if (fight->attack(p, NPCCharacter) == 1) {
						ClearScreen();
						OutputFile("Death.dat");
						std::cout << "You have won!\nYou find nothing on the corpse." << std::endl;
						PressEnter();
					}
					else {
						isOver();
					}
				}

				ClearScreen();
				OutputFile("MoveTen-GeneralHallway.dat");
				PressEnter();
				//:: Move to the King's Door
				std::string kingAnswer = "";
				ClearScreen();
				OutputFile("MoveTen-KingRoom.dat");

				std::cout << "Do you wish to Continue or Run?" << std::endl;
				std::cout << "Your Choice: ";
				std::cin >> kingAnswer;
				kingAnswer[0] = toupper(kingAnswer[0]);
				while (kingAnswer != "Continue" && kingAnswer != "Run") {
					std::cout << kingAnswer << " was not a valid answer. Do you wish to \"Continue\" or \"Run\"?" << std::endl;
					std::cout << "Your Choice: ";
					std::cin >> kingAnswer;
					kingAnswer[0] = toupper(kingAnswer[0]);
				}
				if (kingAnswer == "Run") {
					std::string runConfirm = "";
					std::cout << "Are you sure? Yes or No?" << std::endl;
					std::cout << "Your Answer: ";
					std::cin >> runConfirm;
					runConfirm[0] = toupper(runConfirm[0]);
					while (runConfirm != "Yes" && runConfirm != "No") {
						std::cout << "Do you really want to run? \"Yes\" or \"No\"." << std::endl;
						std::cout << "Your Choice: ";
						std::cin >> runConfirm;
						runConfirm[0] = toupper(runConfirm[0]);
					}
					//:: You're getting a huge fight one way or another. This Engagement is meant to kill the player.
					if (runConfirm == "Yes") {
						ClearScreen();
						OutputFile("MoveTen-RunFromKing.dat");
						std::cout << "FIGHT" << std::endl;
						PressEnter();
						NPCCharacter->setPlayerType("Gargoyle");
						NPCCharacter->setName("Goliath");
						NPCCharacter = new GoliathDecorator(NPCCharacter);
						NPCCharacter = new ChainMailDecorator(NPCCharacter);
						NPCCharacter = new AmuletDecorator(NPCCharacter);
						NPCCharacter = new SwordDecorator(NPCCharacter);

						Actions* fight = new Actions();
						if (fight->attack(p, NPCCharacter) == 1) {
							ClearScreen();
							OutputFile("Death.dat");
							std::cout << "You have won!\nYou find nothing on the corpse." << std::endl;
							PressEnter();
						}
						else {
							isOver();
						}
						//:: Proceed to finish the game. This outcome will be unlikely, but must be addressed
						FinishGame();

					}
					if (runConfirm == "No") {
						kingAnswer = "Continue";
					}
					//:: Proceed to last fight.
				
					
				}
				if (kingAnswer == "Continue") {
					OutputFile("MoveTen-King.dat");
					NPCCharacter = new KingDecorator(NPCCharacter);
					Actions *fight = new Actions();

					if (fight->attack(p, NPCCharacter) == 1) {
						ClearScreen();
						OutputFile("DeathKing.dat");
						std::cout << "You have won!\nYou have killed the Evil King! " << std::endl;
						PressEnter();
					}
					else {
						isOver();

					}
				}
			}
		}
		//:: You're dead Jim
		else {
			ClearScreen();
			OutputFile("Death.dat");
			std::cout << "As you near the top of the ladder, you hear a heart-wrenching crack from beneath your feet.\nThe ladder step you are standing on breaks from your weight, and you fall to your death" << std::endl;
			std::cout << "Game Over!";
			PressEnter();
			kill();

		}
	}
}

void FantasyGame::FinishGame() {
	ClearScreen();
	OutputFile("Credits.dat");
	PressEnter();
	isOver();

}

// Debug code builds a character for testing
void FantasyGame::DebugInitWarrior()
{
	p->setName("Matt");
	p->setPlayerType("Warrior");
	p = new SwordDecorator(p);
	p = new SwordDecorator(p);
}

// Debug code builds a character for testing
void FantasyGame::DebugInitWizard()
{
	p->setName("Matt");
	p->setPlayerType("Wizard");
	p = new StaffDecorator(p);
}

void FantasyGame::DrawStats(Character* pChar)
{
	ClearScreen();
	std::cout << std::endl << "----------------------------------------------------------------------------" << std::endl;
	std::cout << pChar->getName() << " Health: " << pChar->getHealth() << std::endl;
	std::cout << "Attack Power: " << pChar->getAttack() << std::endl;
	std::cout << "Defense Power: " << pChar->getDefense() << std::endl;
	std::cout << std::endl << "----------------------------------------------------------------------------" << std::endl;
}