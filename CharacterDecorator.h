/**
* Filename: CharacterDecorator.h
* Purpose:	To act as a decorator class for both PlayerCharacter's and NPC's.
* NOTE:		Implements the Decorator design pattern, and works closesly with Character.
*
* FURTHER NOTE: This class contains several decorator classes. All methods related to the decorators are 
*				being delcared AND implemented here.
* Group E Productions
*
* Contributors:
*		Matt Koenig
*		Claire Fritzler
*		Tylor Faoro
*/
#pragma once
#include "Character.h"
#include <iostream>

class CharacterDecorator : public Character
{
    Character* decoratedCharcter;
public:
	/**
	* Returns the concrete character being decorated
	*/
	Character* getDecoratedCharacer()
	{
		return decoratedCharcter;
	}
	/**
	* Purpose: To set the passed in character and set to the decoratedCharacter instance variable
	*/
	CharacterDecorator(Character* decoratedCharacterRef)
	{
		decoratedCharcter = decoratedCharacterRef;
	}

	~CharacterDecorator()
	{
		delete decoratedCharcter;
	}
	/**
	* Returns description of decorated item what it is.
	*/
	std::string getDescription()
	{
		return decoratedCharcter->getDescription();
	}
	/**
	* Returns the string action describing what an item does.
	*/
	std::string getAction()
	{
		return decoratedCharcter->getAction();
	}
	/**
	* Returns a multiple of the attack value of an item for a critical attack
	*/
	int criticalAttack(){

		return (decoratedCharcter->getAttack() * 2) ;
	}
	/**
	* Sets the health value of a character
	*/
	void setHealth(int health_value)
	{
		decoratedCharcter->setHealth(health_value);
	}
	/**
	* Sets the amount of health buffs available
	*/
	void setHealthBuffs(int healthbuff_value)
	{
		decoratedCharcter->setHealthBuffs(healthbuff_value);
	}
	/**
	* Returns the health buffs available in all decorated items.
	*/
	int getHealthBuffs()
	{
		return decoratedCharcter->getHealthBuffs();
	}
	/**
	* Use of health potions increases health by the amount of stored health buffs
	*/
	void useCurrentHealthBuffs()
	{
		int UpdatedHealth = decoratedCharcter->getHealth() + getHealthBuffs();
		std::cout << decoratedCharcter->getHealth() << " " << getHealthBuffs() << std::endl;
		decoratedCharcter->setHealth(UpdatedHealth);
	}
	/**
	* Returns the current health of the decorated character.
	*/
	int getHealth()
	{
		return decoratedCharcter->getHealth();
	}
	/**
	* Returns the combined attack power sum of all equiped items.
	*/
	int getAttack()
	{
		return decoratedCharcter->getAttack(); 
	}
	/**
	* Returns the combined defense power sum of all equiped items.
	*/
	int getDefense()
	{
		return decoratedCharcter->getDefense();
	}
	/**
	* Returns the type of the decorated character.
	*/
	std::string getCharacterType()
	{
		return decoratedCharcter->getCharacterType();
	}
	/**
	* Sets the type of the decorated character.
	*/
	void setPlayerType(std::string playertype)
	{
		decoratedCharcter->setPlayerType(playertype);
	}
	/**
	* Sets the name of the decorated character.
	*/
	void setName(std::string strName)
	{
		decoratedCharcter->setName(strName);
	}
	/**
	* Returns the name of the character.
	*/
	std::string getName()
	{
		return decoratedCharcter->getName();
	}
	/**
	* Returns true if the player has at least one key. Allows character to open doors.
	*/
	bool getUnlock()
	{
		return decoratedCharcter->getUnlock();
	}
private:
    std::string PlayerType;
};

/** 
* Class: SwordDecorator
* Purpose:
*	- Concrete implimentation of item Sword.
*	- Stats can be seen in public constructor.
*/
class SwordDecorator : public CharacterDecorator
{
public:
	SwordDecorator(Character* decoratedCharcter) :CharacterDecorator(decoratedCharcter)
	{
		Description = "A sword";
		Action = "Swing";
		Attack = 15;
		Defense = 10;
		HealthBuff = 0;
		Unlock = false;
	}

	int criticalAttack(){

		return (Attack * 2);
	}
	std::string getDescription()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		std::string TempDescriptions = tempChar->getDescription();
		return TempDescriptions.append(" ").append(Description);
	}
	std::string getAction()
	{
		return Action;
	}

	int getAttack()	{		
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempAttack = tempChar->getAttack();
		return Attack + TempAttack;
	}

	int getDefense(){
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempDefense = tempChar->getDefense();
		return Defense + TempDefense;
	}
	int getHealthBuffs()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempHealthBuffs = tempChar->getHealthBuffs();
		return HealthBuff + TempHealthBuffs;
	}
	bool getUnlock()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		bool TempUnlockBool = tempChar->getUnlock();
		if (TempUnlockBool == true)
		{
			return TempUnlockBool;
		}
		else
		{
			return Unlock;
		}
	}
private:
	std::string Description;
	std::string Action;
	int Attack;
	int Defense;
	int HealthBuff;
	bool Unlock;
};
/**
* Class: StaffDecorator
* Purpose:
*	- Concrete implimentation of item Staff.
*	- Stats can be seen in public constructor.
*/
class StaffDecorator : public CharacterDecorator
{
public:
	StaffDecorator(Character* decoratedCharcter) :CharacterDecorator(decoratedCharcter)
	{
		Description = "A staff of conjuring";
		Action = "fires lightning";
		Attack = 20;
		Defense = 7;
		HealthBuff = 0;
		Unlock = false;
	}
	std::string getDescription()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		std::string TempDescriptions = tempChar->getDescription();
		return TempDescriptions.append(" ").append(Description);
	}
	std::string getAction()
	{
		return Action;
	}
	
	int getAttack()	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempAttack = tempChar->getAttack();
		return Attack + TempAttack;
	}
	int getHealthBuffs()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempHealthBuffs = tempChar->getHealthBuffs();
		return HealthBuff + TempHealthBuffs;
	}

	int getDefense()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempDefense = tempChar->getDefense();
		return Defense + TempDefense;
	}
	bool getUnlock()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		bool TempUnlockBool = tempChar->getUnlock();
		if (TempUnlockBool == true)
		{
			return TempUnlockBool;
		}
		else
		{
			return Unlock;
		}
	}
private:
	std::string Description;
	std::string Action;
	int Attack;
	int Defense;
	int HealthBuff;
	bool Unlock;
};
/**
* Class: ClubDecorator
* Purpose:
*	- Concrete implimentation of item Club.
*	- Stats can be seen in public constructor.
*/
class ClubDecorator : public CharacterDecorator
{
public:
	ClubDecorator(Character* decoratedCharcter) :CharacterDecorator(decoratedCharcter)
	{
		Description = "A gnarled club";
		Action = "swings";
		Attack = 10;
		Defense = 7;
		HealthBuff = 0;
		Unlock = false;
	}
	std::string getDescription()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		std::string TempDescriptions = tempChar->getDescription();
		return TempDescriptions.append(" ").append(Description);
	}
	std::string getAction()
	{
		return Action;
	}
	int getAttack()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempAttack = tempChar->getAttack();
		return Attack + TempAttack;
	}
	int getDefense()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempDefense = tempChar->getDefense();
		return Defense + TempDefense;
	}
	int getHealthBuffs()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempHealthBuffs = tempChar->getHealthBuffs();
		return HealthBuff + TempHealthBuffs;
	}
	bool getUnlock()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		bool TempUnlockBool = tempChar->getUnlock();
		if (TempUnlockBool == true)
		{
			return TempUnlockBool;
		}
		else
		{
			return Unlock;
		}
	}
private:
	std::string Description;
	std::string Action;
	int Attack;
	int Defense;
	int HealthBuff;
	bool Unlock;
};
/**
* Class: HealthPotionDecorator
* Purpose:
*	- Concrete implimentation of item Health Potion.
*	- Stats can be seen in public constructor.
*/
class HealthPotionDecorator : public CharacterDecorator
{
public:
	HealthPotionDecorator(Character* decoratedCharcter) :CharacterDecorator(decoratedCharcter)
	{
		Description = "Health Potion";
		Action = "drink";
		Attack = 0;
		Defense = 0;
		HealthBuff = 50;
		Unlock = false;
	}
	std::string getDescription()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		std::string TempDescriptions = tempChar->getDescription();
		return TempDescriptions.append(" ").append(Description);
	}
	std::string getAction()
	{
		return Action;
	}
	int getAttack()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempAttack = tempChar->getAttack();
		return Attack + TempAttack;
	}
	int getDefense()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempDefense = tempChar->getDefense();
		return Defense + TempDefense;
	}
	int getHealthBuffs()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempHealthBuffs = tempChar->getHealthBuffs();
		return HealthBuff + TempHealthBuffs;
	}
	bool getUnlock()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		bool TempUnlockBool = tempChar->getUnlock();
		if (TempUnlockBool == true)
		{
			return TempUnlockBool;
		}
		else
		{
			return Unlock;
		}
	}
private:
	std::string Description;
	std::string Action;
	int Attack;
	int Defense;
	int HealthBuff;
	bool Unlock;
};
/**
* Class: UsedHealthPotionDecorator
* Purpose:
*	- Concrete implimentation of item used health potion.
*   - Cancels out effect of other equiped health potions.
*	- Stats can be seen in public constructor.
*/
class UsedHealthPotionDecorator : public CharacterDecorator
{
public:
	UsedHealthPotionDecorator(Character* decoratedCharcter) :CharacterDecorator(decoratedCharcter)
	{
		Description = "The effect of the potion has been used";
		Action = "You feel empty";
		Attack = 0;
		Defense = 0;
		HealthBuff = -50;
		Unlock = false;
	}
	std::string getDescription()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		std::string TempDescriptions = tempChar->getDescription();
		return TempDescriptions.append(" ").append(Description);
	}
	std::string getAction()
	{
		return Action;
	}
	int getAttack()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempAttack = tempChar->getAttack();
		return Attack + TempAttack;
	}
	int getDefense()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempDefense = tempChar->getDefense();
		return Defense + TempDefense;
	}
	int getHealthBuffs()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempHealthBuffs = tempChar->getHealthBuffs();
		return HealthBuff + TempHealthBuffs;
	}
	bool getUnlock()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		bool TempUnlockBool = tempChar->getUnlock();
		if (TempUnlockBool == true)
		{
			return TempUnlockBool;
		}
		else
		{
			return Unlock;
		}
	}
private:
	std::string Description;
	std::string Action;
	int Attack;
	int Defense;
	int HealthBuff;
	bool Unlock;
};
/**
* Class: FireDecorator
* Purpose:
*	- Concrete implimentation of Fire used to buff the dragon.
*	- Stats can be seen in public constructor.
*/
class FireDecorator : public CharacterDecorator
{
public:
	FireDecorator(Character* decoratedCharcter) :CharacterDecorator(decoratedCharcter)
	{
		Description = "Burning hot fire";
		Action = "burns";
		Attack = 18;
		Defense = 12;
		HealthBuff = 0;
		Unlock = false;
	}
	std::string getDescription()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		std::string TempDescriptions = tempChar->getDescription();
		return TempDescriptions.append(" ").append(Description);
	}
	std::string getAction()
	{
		return Action;
	}
	int getAttack()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempAttack = tempChar->getAttack();
		return Attack + TempAttack;
	}
	int getDefense()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempDefense = tempChar->getDefense();
		return Defense + TempDefense;
	}
	int getHealthBuffs()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempHealthBuffs = tempChar->getHealthBuffs();
		return HealthBuff + TempHealthBuffs;
	}
	bool getUnlock()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		bool TempUnlockBool = tempChar->getUnlock();
		if (TempUnlockBool == true)
		{
			return TempUnlockBool;
		}
		else
		{
			return Unlock;
		}
	}
private:
	std::string Description;
	std::string Action;
	int Attack;
	int Defense;
	int HealthBuff;
	bool Unlock;
};
/** 
* Class: ChainMailDecorator
* Purpose:
*	- Concrete implimentation of item Chain Mail.
*	- Stats can be seen in public constructor.
*/
class ChainMailDecorator : public CharacterDecorator
{
public:
	ChainMailDecorator(Character* decoratedCharcter) :CharacterDecorator(decoratedCharcter)
	{
		Description = "Chain Mail";
		Action = "Increases defense";
		Attack = 0;
		Defense = 10;
		HealthBuff = 0;
		Unlock = false;
	}
	std::string getDescription()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		std::string TempDescriptions = tempChar->getDescription();
		return TempDescriptions.append(" ").append(Description);
	}
	std::string getAction()
	{
		return Action;
	}
	int getAttack()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempAttack = tempChar->getAttack();
		return Attack + TempAttack;
	}
	int getDefense()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempDefense = tempChar->getDefense();
		return Defense + TempDefense;
	}
	int getHealthBuffs()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempHealthBuffs = tempChar->getHealthBuffs();
		return HealthBuff + TempHealthBuffs;
	}
	bool getUnlock()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		bool TempUnlockBool = tempChar->getUnlock();
		if (TempUnlockBool == true)
		{
			return TempUnlockBool;
		}
		else
		{
			return Unlock;
		}
	}
private:
	std::string Description;
	std::string Action;
	int Attack;
	int Defense;
	int HealthBuff;
	bool Unlock;
};
/**
* Class: KeyDecorator
* Purpose:
*	- Concrete implimentation of item Key allows unlocking door.
*	- Stats can be seen in public constructor.
*/
class KeyDecorator : public CharacterDecorator
{
public:
	KeyDecorator(Character* decoratedCharcter) :CharacterDecorator(decoratedCharcter)
	{
		Description = "Key";
		Action = "Unlock";
		Attack = 0;
		Defense = 0;
		HealthBuff = 0;
		Unlock = true;
	}
	std::string getDescription()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		std::string TempDescriptions = tempChar->getDescription();
		return TempDescriptions.append(" ").append(Description);
	}
	std::string getAction()
	{
		return Action;
	}
	int getAttack()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempAttack = tempChar->getAttack();
		return Attack + TempAttack;
	}
	int getDefense()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempDefense = tempChar->getDefense();
		return Defense + TempDefense;
	}
	int getHealthBuffs()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempHealthBuffs = tempChar->getHealthBuffs();
		return HealthBuff + TempHealthBuffs;
	}
	bool getUnlock()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		bool TempUnlockBool = tempChar->getUnlock();
		if (TempUnlockBool == true)
		{
			return TempUnlockBool;
		}
		else
		{
			return Unlock;
		}
	}
private:
	std::string Description;
	std::string Action;
	int Attack;
	int Defense;
	int HealthBuff;
	bool Unlock;
};
/**
* Class: HelmetDecorator
* Purpose:
*	- Concrete implimentation of item Helmet buffs defense.
*	- Stats can be seen in public constructor.
*/
class HelmetDecorator : public CharacterDecorator
{
public:
	HelmetDecorator(Character* decoratedCharcter) :CharacterDecorator(decoratedCharcter)
	{
		Description = "Steel Helmet";
		Action = "Defend";
		Attack = 0;
		Defense = 10;
		HealthBuff = 0;
		Unlock = false;
	}
	std::string getDescription()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		std::string TempDescriptions = tempChar->getDescription();
		return TempDescriptions.append(" ").append(Description);
	}
	std::string getAction()
	{
		return Action;
	}
	int getAttack()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempAttack = tempChar->getAttack();
		return Attack + TempAttack;
	}
	int getDefense()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempDefense = tempChar->getDefense();
		return Defense + TempDefense;
	}
	int getHealthBuffs()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempHealthBuffs = tempChar->getHealthBuffs();
		return HealthBuff + TempHealthBuffs;
	}
	bool getUnlock()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		bool TempUnlockBool = tempChar->getUnlock();
		if (TempUnlockBool == true)
		{
			return TempUnlockBool;
		}
		else
		{
			return Unlock;
		}
	}
private:
	std::string Description;
	std::string Action;
	int Attack;
	int Defense;
	int HealthBuff;
	bool Unlock;
};
/**
* Class: DaggerDecorator
* Purpose:
*	- Concrete implimentation of item Dagger buffs damage.
*	- Stats can be seen in public constructor.
*/
class DaggerDecorator : public CharacterDecorator
{
public:
	DaggerDecorator(Character* decoratedCharcter) :CharacterDecorator(decoratedCharcter)
	{
		Description = "Dagger";
		Action = "Attack";
		Attack = 10;
		Defense = 2;
		HealthBuff = 0;
		Unlock = false;
	}
	std::string getDescription()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		std::string TempDescriptions = tempChar->getDescription();
		return TempDescriptions.append(" ").append(Description);
	}
	std::string getAction()
	{
		return Action;
	}
	int getAttack()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempAttack = tempChar->getAttack();
		return Attack + TempAttack;
	}
	int getDefense()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempDefense = tempChar->getDefense();
		return Defense + TempDefense;
	}
	int getHealthBuffs()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempHealthBuffs = tempChar->getHealthBuffs();
		return HealthBuff + TempHealthBuffs;
	}
	bool getUnlock()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		bool TempUnlockBool = tempChar->getUnlock();
		if (TempUnlockBool == true)
		{
			return TempUnlockBool;
		}
		else
		{
			return Unlock;
		}
	}
private:
	std::string Description;
	std::string Action;
	int Attack;
	int Defense;
	int HealthBuff;
	bool Unlock;
};
/**
* Class: ShieldDecorator
* Purpose:
*	- Concrete implimentation of item Shield buffs defense.
*	- Stats can be seen in public constructor.
*/
class ShieldDecorator : public CharacterDecorator
{
public:
	ShieldDecorator(Character* decoratedCharcter) :CharacterDecorator(decoratedCharcter)
	{
		Description = "Shield";
		Action = "Defend";
		Attack = 3;
		Defense = 15;
		HealthBuff = 0;
		Unlock = false;
	}
	std::string getDescription()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		std::string TempDescriptions = tempChar->getDescription();
		return TempDescriptions.append(" ").append(Description);
	}
	std::string getAction()
	{
		return Action;
	}
	int getAttack()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempAttack = tempChar->getAttack();
		return Attack + TempAttack;
	}
	int getDefense()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempDefense = tempChar->getDefense();
		return Defense + TempDefense;
	}
	int getHealthBuffs()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempHealthBuffs = tempChar->getHealthBuffs();
		return HealthBuff + TempHealthBuffs;
	}
	bool getUnlock()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		bool TempUnlockBool = tempChar->getUnlock();
		if (TempUnlockBool == true)
		{
			return TempUnlockBool;
		}
		else
		{
			return Unlock;
		}
	}
private:
	std::string Description;
	std::string Action;
	int Attack;
	int Defense;
	int HealthBuff;
	bool Unlock;
};
/**
* Class: HammerDecorator
* Purpose:
*	- Concrete implimentation of item Hammer buffs attack.
*	- Stats can be seen in public constructor.
*/
class HammerDecorator : public CharacterDecorator
{
public:
	HammerDecorator(Character* decoratedCharcter) :CharacterDecorator(decoratedCharcter)
	{
		Description = "Blacksmith Hammer";
		Action = "Attack";
		Attack = 3;
		Defense = 2;
		HealthBuff = 0;
		Unlock = false;
	}
	std::string getDescription()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		std::string TempDescriptions = tempChar->getDescription();
		return TempDescriptions.append(" ").append(Description);
	}
	std::string getAction()
	{
		return Action;
	}
	int getAttack()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempAttack = tempChar->getAttack();
		return Attack + TempAttack;
	}
	int getDefense()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempDefense = tempChar->getDefense();
		return Defense + TempDefense;
	}
	int getHealthBuffs()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempHealthBuffs = tempChar->getHealthBuffs();
		return HealthBuff + TempHealthBuffs;
	}
	bool getUnlock()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		bool TempUnlockBool = tempChar->getUnlock();
		if (TempUnlockBool == true)
		{
			return TempUnlockBool;
		}
		else
		{
			return Unlock;
		}
	}
private:
	std::string Description;
	std::string Action;
	int Attack;
	int Defense;
	int HealthBuff;
	bool Unlock;
};
/**
* Class: BastardSwordDecorator
* Purpose:
*	- Concrete implimentation of item Bastard Sword buffs attack for warrior.
*	- Stats can be seen in public constructor.
*/
class BastardSwordDecorator : public CharacterDecorator
{
public:
	BastardSwordDecorator(Character* decoratedCharcter) :CharacterDecorator(decoratedCharcter)
	{
		Description = "Blacksmith Hammer";
		Action = "Attack";
		Attack = 30;
		Defense = 25;
		HealthBuff = 0;
		Unlock = false;
	}
	std::string getDescription()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		std::string TempDescriptions = tempChar->getDescription();
		return TempDescriptions.append(" ").append(Description);
	}
	std::string getAction()
	{
		return Action;
	}
	int getAttack()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempAttack = tempChar->getAttack();
		return Attack + TempAttack;
	}
	int getDefense()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempDefense = tempChar->getDefense();
		return Defense + TempDefense;
	}
	int getHealthBuffs()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempHealthBuffs = tempChar->getHealthBuffs();
		return HealthBuff + TempHealthBuffs;
	}
	bool getUnlock()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		bool TempUnlockBool = tempChar->getUnlock();
		if (TempUnlockBool == true)
		{
			return TempUnlockBool;
		}
		else
		{
			return Unlock;
		}
	}
private:
	std::string Description;
	std::string Action;
	int Attack;
	int Defense;
	int HealthBuff;
	bool Unlock;
};
/**
* Class: AmuletDecorator
* Purpose:
*	- Concrete implimentation of item Amulet buffs attack and defense.
*	- Stats can be seen in public constructor.
*/
class AmuletDecorator : public CharacterDecorator
{
public:
	AmuletDecorator(Character* decoratedCharcter) :CharacterDecorator(decoratedCharcter)
	{
		Description = "Magic Amulet";
		Action = "Buff";
		Attack = 10;
		Defense = 10;
		HealthBuff = 0;
		Unlock = false;
	}
	std::string getDescription()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		std::string TempDescriptions = tempChar->getDescription();
		return TempDescriptions.append(" ").append(Description);
	}
	std::string getAction()
	{
		return Action;
	}
	int getAttack()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempAttack = tempChar->getAttack();
		return Attack + TempAttack;
	}
	int getDefense()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempDefense = tempChar->getDefense();
		return Defense + TempDefense;
	}
	int getHealthBuffs()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempHealthBuffs = tempChar->getHealthBuffs();
		return HealthBuff + TempHealthBuffs;
	}
	bool getUnlock()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		bool TempUnlockBool = tempChar->getUnlock();
		if (TempUnlockBool == true)
		{
			return TempUnlockBool;
		}
		else
		{
			return Unlock;
		}
	}
private:
	std::string Description;
	std::string Action;
	int Attack;
	int Defense;
	int HealthBuff;
	bool Unlock;
};
/**
* Class: GoliathDecorator
* Purpose:
*	- Concrete implimentation of item Goliath decorator meant to thwomp the player until dead.
*	- Stats can be seen in public constructor.
*/
class GoliathDecorator : public CharacterDecorator
{
public:
	GoliathDecorator(Character* decoratedCharcter) :CharacterDecorator(decoratedCharcter)
	{
		Description = "Goliath's Slam";
		Action = "Attack";
		Attack = 125;
		Defense = 85;
		HealthBuff = 25;
		Unlock = false;
	}
	std::string getDescription()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		std::string TempDescriptions = tempChar->getDescription();
		return TempDescriptions.append(" ").append(Description);
	}
	std::string getAction()
	{
		return Action;
	}
	int getAttack()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempAttack = tempChar->getAttack();
		return Attack + TempAttack;
	}
	int getDefense()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempDefense = tempChar->getDefense();
		return Defense + TempDefense;
	}
	int getHealthBuffs()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempHealthBuffs = tempChar->getHealthBuffs();
		return HealthBuff + TempHealthBuffs;
	}
	bool getUnlock()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		bool TempUnlockBool = tempChar->getUnlock();
		if (TempUnlockBool == true)
		{
			return TempUnlockBool;
		}
		else
		{
			return Unlock;
		}
	}
private:
	std::string Description;
	std::string Action;
	int Attack;
	int Defense;
	int HealthBuff;
	bool Unlock;
};

/**
* Class: KingDecorator
* Purpose:
*	- Concrete implimentation of item King decorator.
*	- Stats can be seen in public constructor.
*/

class KingDecorator : public CharacterDecorator
{
public:
	KingDecorator(Character* decoratedCharcter) :CharacterDecorator(decoratedCharcter)
	{
		Description = "King's Sword";
		Action = "Attack";
		Attack = 125;
		Defense = 85;
		HealthBuff = 25;
		Unlock = false;
	}
	std::string getDescription()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		std::string TempDescriptions = tempChar->getDescription();
		return TempDescriptions.append(" ").append(Description);
	}
	std::string getAction()
	{
		return Action;
	}
	int getAttack()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempAttack = tempChar->getAttack();
		return Attack + TempAttack;
	}
	int getDefense()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempDefense = tempChar->getDefense();
		return Defense + TempDefense;
	}
	int getHealthBuffs()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		int TempHealthBuffs = tempChar->getHealthBuffs();
		return HealthBuff + TempHealthBuffs;
	}
	bool getUnlock()
	{
		Character* tempChar = CharacterDecorator::getDecoratedCharacer();
		bool TempUnlockBool = tempChar->getUnlock();
		if (TempUnlockBool == true)
		{
			return TempUnlockBool;
		}
		else
		{
			return Unlock;
		}
	}
private:
	std::string Description;
	std::string Action;
	int Attack;
	int Defense;
	int HealthBuff;
	bool Unlock;
};
