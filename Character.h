/**
* Filename: PlayerCharacter.h
* Purpose:	PlayerCharacter / NPC Base abstract class declaration
* NOTE:		This class uses the Strategy design pattern. Has two Concrete classes: NPC and PlayerCharacter
* Group E Productions
*
* Contributors:
*		Matt Koenig
*		Claire Fritzler
*		Tylor Faoro
*/
#pragma once
#ifndef CHARACTER_H
#define CHARACTER_H

#include "Game.h"
#include <string>


class Character {
public:
	virtual std::string getDescription() = 0;
	virtual std::string getAction() = 0;
	virtual void setPlayerType(std::string player_type) {};
	virtual std::string getCharacterType() { return NULL; };
	virtual int getHealth() = 0;
	virtual void setHealth(int health_value) = 0;
	virtual void setHealthBuffs(int healthbuff_value) = 0;
	virtual int getHealthBuffs() = 0;
	virtual void useCurrentHealthBuffs() = 0;
	virtual int getAttack() = 0;
	virtual int getDefense() = 0;
	virtual void setName(std::string) = 0;
	virtual std::string getName() = 0;
	virtual bool getUnlock() = 0; // Bool for key use.
	virtual ~Character() {};
	bool attkCrit = false;
	bool defCrit = false;
	Game *g;
private:
	std::string name;
	int health = 100;
	int defense = 10;

protected:
	
};

#endif
