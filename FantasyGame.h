/**
* Filename: FantasyGame.h
* Purpose:	The main framework for the Adventure Land Game. All game "Chapters" are implemented here.
* Group E Productions
*
* Contributors:
*		Matt Koenig
*		Claire Fritzler
*		Tylor Faoro
*/
#pragma once
#ifndef FANTASY_GAME_H
#define FANTASY_GAME_H

#include "Game.h"
#include "Environments.h"
#include "PlayerCharacter.h"
#include "NPC.h"
#include "Actions.h"
#include <string>
#include <fstream>
#include <iostream>
#include <time.h>


class FantasyGame : public Game {
public:
	FantasyGame();

	void play();
	void kill();
	bool isOver();
	void OutputFile(std::string);
	void EnterInput(std::string question, int inputVar);
	void NextMove();

	void InitGame();
	void MoveOne();
	void MoveTwo();
	void MoveThree();
	void MoveFour();
	void MoveFive();
	void MoveSix();
	void MoveSeven();
	void MoveEight();
	void MoveNine();
	void MoveTen();
	void FinishGame();
	Character * p = new PlayerCharacter("Thomas");
	Character * NPCCharacter = new NPC("");
	~FantasyGame();

	void DrawStats(Character*);

	void DebugInitWarrior(); // Remove This is for testing only, builds a Warrior Character.
	void DebugInitWizard(); // Remove This is for testing only, builds a Wizard Character.

	enum { Play, Load, Exit };

	Actions* action;
Game *g;
private:
	
	bool alive;
	int moveOption = 0;
	int moveCounter = 0;	//:: Controls What move the game is currently on { SET TO ZERO ON FINAL }

};

#endif
