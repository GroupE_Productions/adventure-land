/**
* Filename: Environments.h
* Purpose:	Environments class declaration
* Group E Productions
*
* Contributors:
*		Matt Koenig
*		Claire Fritzler
*		Tylor Faoro
*/
#pragma once
#ifndef ENVIRONMENTS_H
#define ENVIRONMENTS_H

#include <string>
#include "Character.h"
#include "Game.h"

class Environments {
public:
	Environments();
	Environments(int envNum);
	Environments(std::string);
	~Environments();
	std::string getEnvName();
	void setEnvName(std::string);
	Character* character;
	
private:
	std::string strEnvironmentName;
	enum locale{DARK_CAVE = 1, CATACOMB = 2, FOREST = 3, SWAMP = 4, DarkForest = 5};
};

#endif
