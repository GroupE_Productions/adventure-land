/**
* Filename: Game.h
* Purpose: Abstract Game Class Declaration. This class establishes utility functions for use by the game.
*		   Contains pure virtuals, so it cannot be instantiated.
* Group E Productions
*
* Contributors:
*		Matt Koenig
*		Claire Fritzler
*		Tylor Faoro
*/
#pragma once
#ifndef GAME_H
#define GAME_H

#include <fstream>
#include <string>
#include <cstdlib>

class Game{
public:
    virtual void play() = 0;
    virtual bool isOver() = 0;
	std::string PrintFileContents(std::ifstream &file);
    void save();
    void load();
	void ClearScreen();
	void PressEnter();
	bool DiceRoll(int seed);
};

#endif

