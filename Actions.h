/**
* Filename: Actions.h
* Purpose:	Action class declaration
* Group E Productions
*
* Contributors:
*		Matt Koenig
*		Claire Fritzler
*		Tylor Faoro
*/

#pragma once
#ifndef ACTIONS_H
#define ACTIONS_H
#include <string>
#include "PlayerCharacter.h"
#include "NPC.h"

class Actions {
public:
	enum direction { Left = 1, Forward = 2, Right = 3 };

	Actions();
	~Actions();
	void ChooseDirection(direction);
	std::string help();
	int attack(Character*, Character*);
	int defend();
	bool checkCrit();
	bool climb();
	void ClearScreen();
	void PressEnter();
	bool climbSuccess;
	Character* getItem(Character*, std::string);
	Game *g;

	std::string randAttackVerb(int);

	bool playerAttackCrit = false;
	bool npcAttackCrit = false;


};

#endif