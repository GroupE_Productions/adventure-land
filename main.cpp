/**
* Filename: main.cpp
* Purpose: This is the file that pulls the magic together.
*
* Game Name:	Adventure Land
* Group Name:	Group E Productions
* Class:		CPSC-2720
* Professor:	Dr. John Anvik
*
* Contributing Programmers:
*		- Matt Koenig
*		- Claire Fritzler
*		- Tylor Faoro
*
* Due Date: November 8th, 2015
*/
#include <iostream>
#include "FantasyGame.h"
#include <string>
using namespace std;

int main(){
string str = "";
    FantasyGame *game = new FantasyGame();
	//game->save();
	//game->InitGame();
	game->play();
	//system("pause");
	
	//delete game;
	return 0;
	
	
}
