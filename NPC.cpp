#include "NPC.h"
#include <string>

/*
* Class constructor for PlayerCharacter
* The user will set the name of the character
* The character initially has full health (I arbitraily chose to be 10)
* and they have no items
*/
NPC::NPC(std::string playerName) {
	name = playerName;
	health = 10;
	healthbuff = 0;
}


NPC::~NPC() {
}

int NPC::getHealth()
{
	return health;
}

void NPC::setHealth(int health_value)
{
	health = health_value;
}

void NPC::setHealthBuffs(int healthbuff_value)
{
	healthbuff = healthbuff_value;
}


int NPC::getHealthBuffs()
{
	return 0;
}

void NPC::useCurrentHealthBuffs()
{
	health = health + getHealthBuffs();
	healthbuff = healthbuff - healthbuff;

}

void NPC::setName(std::string strName)
{
	name = strName;
}

std::string NPC::getName()
{
	return name;
}

/*
* Function to add an item to the players armoury
* This is just a template for now
* Should the item type be passed in as a parameter?
*/
/*
void NPC::getItem(Item_Types &new_item)
{
	item.push_back(new_item);
}
*/

void NPC::useItem()
{

}

std::string NPC::getNPCType()
{
	return NPCType;
}

void NPC::setNPCType(std::string npctype)
{
	NPCType = npctype;
}

std::string NPC::getDescription()
{
	return NULL;
}

std::string NPC::getAction()
{
	return NULL;
}

int NPC::getAttack()
{
	return 0;
}

int NPC::getDefense()
{
	return 0;
}

bool NPC::getUnlock() // Bool for key use.
{
	return 0;
}

