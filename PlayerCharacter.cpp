#include "PlayerCharacter.h"
#include <string>
#include <iostream>

/*
* Class constructor for PlayerCharacter
* The user will set the name of the character
* The character initially has full health (I arbitraily chose to be 10)
* and they have no items
*/
PlayerCharacter::PlayerCharacter(std::string playerName) {
	name = playerName;
	health = 100;
}


PlayerCharacter::~PlayerCharacter() {
}

int PlayerCharacter::getHealth()
{
	return health;
}

void PlayerCharacter::setHealth(int health_value)
{
	health = health_value;
}

void PlayerCharacter::setHealthBuffs(int healthbuff_value)
{
	//healthbuff = 0 + healthbuff_value;
}

int PlayerCharacter::getHealthBuffs()
{
	return 0;
}

void PlayerCharacter::useCurrentHealthBuffs()
{
	//health = health + getHealthBuffs();
	//healthbuff = healthbuff - healthbuff;
}

void PlayerCharacter::setName(std::string strName)
{
	name = strName;
}

std::string PlayerCharacter::getName()
{
	return name;
}

void PlayerCharacter::useItem()
{

}

std::string PlayerCharacter::getCharacterType()
{
	return PlayerType;
}

void PlayerCharacter::setPlayerType(std::string playertype)
{
	PlayerType = playertype;
}

std::string PlayerCharacter::getDescription()
{
	return Description;
}

std::string PlayerCharacter::getAction()
{
	return NULL;
}

int PlayerCharacter::getAttack()
{
	return 0;
}

int PlayerCharacter::getDefense()
{
	return 0;
}

bool PlayerCharacter::getUnlock() // Bool for key use.
{
	return 0;
}


