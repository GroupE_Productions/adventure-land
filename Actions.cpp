/**
* Filename: Actions.cpp
* Purpose:	Provides implementation methods for the Actions class.
*
* Group E Productions
*
* Contributors:
*		Matt Koenig
*		Tylor Faoro
*/
#include "Actions.h"
#include <string>
#include <iostream>
#include <time.h>
#ifdef _WIN32
	#include <conio.h>	
#else
	#include <curses.h>
#endif

Actions::Actions() {
}


Actions::~Actions() {
}

std::string Actions::help()
{
	std::string mystring("The Return");
	return mystring;
}

bool Actions::checkCrit(){

	/*bool pResult = g->DiceRoll(15);
		bool nResult = g->DiceRoll(15);
		if (pResult){
			this->playerAttackCrit = true;
		}
		else{
			this->playerAttackCrit = false;
		}*/
		/*if (nResult){
			this->npcAttackCrit = true;
		}
		else{
			this->npcAttackCrit = false;

		}*/

		//std::cout << "Player Attack Crit: " << this->playerAttackCrit;
		//std::cout << std::endl << "NPC Attack Crit " << this->npcAttackCrit << std::endl;

		return playerAttackCrit;	
}

/**
* Purpose:	This method acts as the main attack mechanic of the entire game. This is where attack values, defence,
*			and net damage are calculated. This method makes heavy use of Decorator methods contained within the Character
*			Decorator Class. This method will check for normal attacks, and critical strikes and updated the UI to indicate 
*			such damage.
*
* NOTE:		MAJOR COMPONENT
* 
* Contributors:
*			Matt Koenig
*			Tylor Faoro
*/
int Actions::attack(Character* pChar, Character* nChar)
{
	//:: Acquire NPC and Player Health
	int PlayerHealth = pChar->getHealth();
	int NPCHealth = nChar->getHealth(); //:: @TODO Raised NPC Health to test crit function. Ensure you remove.

	//:: String Literals that will be concatenated to attack/defense to show Critical Hit or Strong Defence
	std::string criticalAttack = " (CRITICAL STRIKE)";
	std::string strongDefence  = " (STRONG DEFENCE)";

	
	//:: Running while loop with bool instead of checks to simplify conditions. Checks are now being done inside loop.
	bool runLoop = true;
	int intRountCounter = 1;
	while (runLoop){
		ClearScreen();

		//:: Round one, FIGHT!
		std::cout << std::endl << std::endl << "You choose to fight!       - Rount " << intRountCounter << " Begin!" << std::endl << "----------------------------------------------------------------------------" << std::endl << std::endl;
		std::cout << pChar->getName() << " Health: " << PlayerHealth << "               " << nChar->getName() << " Health: " << NPCHealth << std::endl;
		std::cout << "Attack Power: " << pChar->getAttack() << "               " << "Attack Power: " << nChar->getAttack() << std::endl;
		std::cout << "Defense Power: " << pChar->getDefense() << "               " << "Defense Power: " << nChar->getDefense() << std::endl;
		std::cout << std::endl << "----------------------------------------------------------------------------" << std::endl;
		/*
		* Determines attack value. 
		* Formulae: New Target HP Level == Targets CURRENT HP Level - (Attackers Attack value - Defenders Defense Value)
		*/

		srand(time(0) + 42);
		int PlayerRandVar = (rand() % 2);
		PlayerRandVar++;
		//std::cout << "Player Rand Var: " << PlayerRandVar << std::endl;

		srand(time(0) + 37);
		int NPCRandVar = (rand() % 2);
		NPCRandVar++;
		//std::cout << "NPC Rand Var: " << NPCRandVar << std::endl;

		//std::cout << "NPC Health:" << NPCHealth << " Player Attack: " << pChar->getAttack() << " NPC Defense: " << nChar->getDefense() << std::endl;
		int NPCDamage = ((pChar->getAttack() * PlayerRandVar) - nChar->getDefense());
		//std::cout << "NPC Damage: " << NPCDamage;
		if (NPCDamage < 0)
		{
			NPCHealth = NPCHealth;
		}
		else
		{
			NPCHealth = NPCHealth - NPCDamage;
		}

		//std::cout << "Player Health: " << PlayerHealth << " NPC Attack: " << nChar->getAttack() << " Player Defense: " << pChar->getDefense() << std::endl;
		int PlayerDamage = ((nChar->getAttack() * NPCRandVar) - pChar->getDefense());
		//std::cout << "Player Damage: " << PlayerDamage;
		if (PlayerDamage < 0)
		{
			PlayerHealth = PlayerHealth;
		}
		else
		{
			PlayerHealth = PlayerHealth - PlayerDamage;
		}


		//:: Make sure that our health doesn't display below zero.
		if (NPCHealth < 0 ){
			NPCHealth = 0;
		}
		else if (PlayerHealth < 0){
			PlayerHealth = 0;
		}

		//:: Check if Player has laid a critical strike
		if (pChar->attkCrit){
			std::cout << pChar->getName() << " Attacks!" << std::endl;
			std::cout << pChar->getName() << " " << randAttackVerb(0) << " " << nChar->getName() << " landing a " << criticalAttack << std::endl;
		}
		if (!pChar->attkCrit) {
			std::cout << pChar->getName() << " Attacks!" << std::endl;
			std::cout << pChar->getName() << " " << randAttackVerb(0) << " " << nChar->getName() << std::endl;
		}
		
		PressEnter();
		std::cout << std::endl;

		//:: Check if NPC has laid a critical strike
		if (nChar->attkCrit){
			std::cout << nChar->getName() << " Attacks!" << std::endl;
			std::cout << nChar->getName() << " " << randAttackVerb(1) << " " << pChar->getName() << " landing a " << criticalAttack << std::endl;
			std::cout << "Player Health: " << PlayerHealth << " NPCHealth: " << NPCHealth << std::endl;
		}
		if (!nChar->attkCrit) {
			std::cout << nChar->getName() << " Attacks!" << std::endl;
			std::cout << nChar->getName() << " " << randAttackVerb(1) << " " << pChar->getName() << std::endl;
			std::cout << "Player Health: " << PlayerHealth << " NPCHealth: " << NPCHealth << std::endl;
		}	

		//:: Check if Neither Player NOR NPC has laid a critical strike
		if (!pChar->attkCrit && !nChar->attkCrit){
			std::cout << "Player Health: " << PlayerHealth << " NPCHealth: " << NPCHealth << std::endl;
		}
		
		//:: The Beast has been slain
		if (NPCHealth <= 0){
			NPCHealth = 0;
			std::cout << "You have slain the beast! Nobody will care about what you have done here today." << std::endl;
			PressEnter(); //:: Being used to check if battle conditions a successful
			runLoop = 0;
			
		}
		//:: You're dead Jim!
		else if (PlayerHealth <= 0){
			PlayerHealth = 0;
			std::cout << "You have lost the fight and have died horrendously!" << std::endl;
			PressEnter(); //:: Being used to check if battle conditions a successful
			return 0;
			runLoop = 0;
		}
		PressEnter();
		intRountCounter++;
		
	}

	return 1;
}

/**
* Purpose:	Provides a random chance of successfully climbing something
*
*
* Contributors:
*			Tylor Faoro
*/
bool Actions::climb(){
	bool outcome = g->DiceRoll(5);
	if (!outcome){
		climbSuccess = true;
	}
	else{
		climbSuccess = false;
	}

	return climbSuccess;
}

/**
* Purpose:	Checks which OS is being used and issues an appropriate clear screen command.
*			This method is a Housekeeping Utility method to ensure no clutter on the screen.
*
* Author:	Tylor Faoro
*/
void Actions::ClearScreen() {
#ifdef _WIN32
	std::system("CLS");

#else
	std::system("clear");
#endif
}

/**
* Purpose: Acts as a game pause. Awaits user input then continues through the game.
*
* Author: Matt Koeinig
*/
void Actions::PressEnter() {
	//std::cout << "Press Any Key to continue: ";
	char temp = 'x';
	temp = 'y';
	std::cout << std::endl << "Press Any Key to Continue: ";
	//while (temp != 'c')
		std::cin.get(temp);
}

/**
* Purpose:	Generates an attack verb at random. Batman style.
*
* Author:	Matt Koenig
*/
std::string Actions::randAttackVerb(int seed)
{
	srand(time(0)+seed);
	const std::string wordList[8] = { "lunges at", "swings at ", "strikes ", "clobbers", "slaps", "decisivly kicks", "paper cut", "punches"};

	std::string word = wordList[rand() % 8];
	return word;
}

/**
* Purpose:	Character picks up an item. The item is instantiated for the character. 
*			Then newly decorated character is returned.
*
* Authour: Matt Koenig
*/
Character* Actions::getItem(Character* pChar, std::string strItem)
{
	std::cout << pChar->getName() << " is equiping " << strItem << std::endl;

	if (strItem == "Health Potion")
	{
		pChar = new HealthPotionDecorator(pChar);
		return pChar;
	}
	if (strItem == "Chain Mail Shirt")
	{
		pChar = new ChainMailDecorator(pChar);
		return pChar;
	}
	if (strItem == "Key")
	{
		pChar = new KeyDecorator(pChar);
		return pChar;
	}
	if (strItem == "Sword")
	{
		pChar = new SwordDecorator(pChar);
		return pChar;
	}
	if (strItem == "Staff")
	{
		pChar = new StaffDecorator(pChar);
		return pChar;
	}
	if (strItem == "Club")
	{
		pChar = new ClubDecorator(pChar);
		return pChar;
	}
	if (strItem == "Chain Mail")
	{
		pChar = new ChainMailDecorator(pChar);
		return pChar;
	}
	if (strItem == "Helmet")
	{
		pChar = new HelmetDecorator(pChar);
		return pChar;
	}
	if (strItem == "Dagger")
	{
		pChar = new DaggerDecorator(pChar);
		return pChar;
	}
	if (strItem == "Shield")
	{
		pChar = new ShieldDecorator(pChar);
		return pChar;
	}
	if (strItem == "AmuletDecorator")
	{
		pChar = new AmuletDecorator(pChar);
		return pChar;
	}
	return pChar;
}

