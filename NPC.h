/**
* Filename: NPC.h
* Purpose:	NPC class declaration
* NOTE:		Acting as a concrete class for the character class.
* Group E Productions
*
* Contributors:
*		Matt Koenig
*		Claire Fritzler
*		Tylor Faoro
*/
#pragma once
#ifndef NPC_H
#define NPC_H

#include "Character.h"
#include "CharacterDecorator.h"
#include <string>
#include <vector>

/**
* Class for non-player-characters
* These are characters that the player might encounter in the game
*/

class NPC : public Character {
public:
	/**
	* Constructor for NPC
	* @param name is the name of the character
	*/
	NPC(std::string name);
	/**
	* Destructor of the NPC class
	*/
	~NPC();
	/**
	* @return the health of the NPC character
	*/
	int getHealth();
	/**
	* Method to set the health of the characte
	* @param health_value sets the health of the NPC 
	*/
	void setHealth(int health_value);
	/**
	* 
	*/
	void setHealthBuffs(int healthbuff_value);
	/**
	*
	*/
	int getHealthBuffs();
	/**
	*
	*/
	void useCurrentHealthBuffs();
	/**
	* Method that allows an NPC to use an item
	*/
	void useItem();
	/**
	* @returns the type of the NPC (ie: ogre, dragon, evil king, etc)
	*/
	std::string getNPCType();
	/**
	* Method to set the type of the NPC
	* @param player_type is the type that NPC will be set to
	*/
	void setNPCType(std::string player_type);
	/**
	* virtual method to set name
	* @param n is name
	*/
	virtual void setName(std::string n);
	/**
	* virtual method to @return name of NPC
	*/
	virtual std::string getName();
	/**
	* Method to @return description of NPC
	*/
	std::string getDescription();
	/**
	* Method to @return action of NPC
	*/
	std::string getAction();
	/**
	* @virtual method to @return if character has ability to unlock doors
	*/
	virtual bool getUnlock(); // Bool for key use.
	/**
	* Method to @return attack value
	*/
	int getAttack();
	/**
	* Method to @return defense value of NPC
	*/
	int getDefense();
private:
	int health; /*< Health value of the NPC */
	int healthbuff; /*< Health buffer value of NPC */
	bool crit; /*< Crit value determines if NPC is capable of critical attack */
	std::string name; /*< Name of the NPC */
	std::string NPCType; /*< Type of the NPC */

};

#endif

