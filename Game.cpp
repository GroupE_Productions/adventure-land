#include "Game.h"
#include "Character.h"
#include <iostream>
#ifdef _WIN32
	#include <conio.h>	
#else
	#include <curses.h>
#endif

/*Game::Game(){
}*/

/**
* Purpose:	Take in an external file, scan the document, then output data to screen.
*			Method also performs existential testing on the file.
*
* Author:	Tylor Faoro
*/
std::string Game::PrintFileContents(std::ifstream &File){
	std::string Lines = "";        //All lines	

	if (File){
		while (File.good()){
			std::string TempLine;                  //Temp line
			std::getline(File, TempLine);        //Get temp line
			TempLine += "\n";                      //Add newline character

			Lines += TempLine;                     //Add newline
		}
		return  Lines;
	}
	else{
		return "ERROR File does not exist.";
	}
}

/**
* Purpose:	A simple Dice Rolling method. Takes a seed which stands as the percentage of success.
* Return:	Returns True if the roll was successful, False otherwise.
* 
* Author:	Tylor Faoro
*/
bool Game::DiceRoll(int seed){
	bool roll = (rand() % 100 ) < seed;

	return roll;

}

/**
* Purpose:	Checks which OS is being used and issues an appropriate clear screen command.
*			This method is a Housekeeping Utility method to ensure no clutter on the screen.
*
* Author:	Tylor Faoro
*/
void Game::ClearScreen(){
#ifdef _WIN32
	std::system("CLS");
	
#else
	std::system("clear");
#endif
}

/**
* Purpose:	Acts as a game pause. Awaits user input then continues through the game.
*
* Author:	Matt Koeinig
*/
void Game::PressEnter() {
	//std::cout << "Press Any Key to continue: ";
	//getch();
	
	char temp = 'x';
	temp = 'y';
	std::cout << std::endl << "Press Any Key to Continue: ";
	//while (temp != 'c')
		std::cin.get(temp);
		
}

void Game::save(){
    std::ofstream o;
    o.open("../adventure-land/Assets/loadFile.txt", std::ios::app);
    if ( !o.is_open() ){
        std::cout << "The SAVE file failed to load." << std::endl;
    }
    else{
        std::cout << "The SAVE file has been loaded." << std::endl;
        o << "Character Class: " << "\tCurrent Phase: "<< std::endl;
		o << "Warrior" << "\t\t\t\t\tPhase 1" << std::endl;
    }
    o.close();
}

void Game::load(){
    std::ifstream loadFile;
    loadFile.open("../adventure-land/Assets/loadFile.txt");
    if (!loadFile.is_open() ){
        std::cout << "The LOAD file failed to load." << std::endl;
    }
    else{
        std::cout << "the LOAD file has been loaded." << std::endl;
        
    }
    
    
    
    
    loadFile.close();
}
