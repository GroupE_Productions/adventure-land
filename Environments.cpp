/**
* Filename: Environments.cpp
* Purpose:	Provides implementation methods for the Environments Class.
*
* Group E Productions
*
* Contributors:
*		Matt Koenig
*		Claire Fritzler
*		Tylor Faoro
*/
#include "Environments.h"
#include "Game.h"
#include <string>

/**
* Purpose:	Default Constructor. Sets the Environment type to "Defaults".*
*/
Environments::Environments() {
	setEnvName("Defaults");
}

/**
* Purpose:	Overloaded constructor. Takes in a pseudo-random number and generates the
*			environment type that is associated with that int value.
*
* Author:	Tylor Faoro
*/
Environments::Environments(int envNum){

	switch (envNum){
	default:
		break;
	case DARK_CAVE:
		setEnvName("Dark Cave");
		break;

	case CATACOMB:
		setEnvName("Catacomb");
		break;

	case FOREST:
		setEnvName("Forest");
		break;

	case SWAMP:
		setEnvName("Swamp");
		break;
	}
}

/**
* Purpose:  Overloaded Constructor. Takes in a string literal, so developer can set a static 
*			environment.
*
* Author:	Matt Koenig
*/
Environments::Environments(std::string strEnvName)
{
	setEnvName(strEnvName);
}

/**
* Purpose: Accessor for the Environment name variable.
*/
std::string Environments::getEnvName()
{
	return strEnvironmentName;
}

/**
* Purpose: Mutator for the Environment name variable.*
*/
void Environments::setEnvName(std::string strEnvName)
{
	strEnvironmentName = strEnvName;
}

/**
* Purpose: Destructor.
*/
Environments::~Environments() {
}
